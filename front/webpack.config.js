const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const webpack = require('webpack'); //to access built-in plugins
const path = require('path');

module.exports = {
    entry: './app/app.js',
    output: {
        filename: 'microblog.bundle.js',
        path: path.resolve(__dirname, '../api/public')
    },
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(png|jpg)$/,
                loader: 'url-loader'
            },
            {
                test: /\.txt/i,
                use: 'raw-loader',
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({template: './app/index.html'})
    ]
};