'use strict';

require("./lib/angular/angular.js");
require("./lib/angular-sanitize/angular-sanitize.min.js");
require("./lib/angular-cookies/angular-cookies.min.js");
require("./lib/angular-md5/angular-md5.js");
require('imports-loader?this=>window!./lib/html5-boilerplate/dist/js/vendor/modernizr-2.8.3.min.js');
require("./lib/ng-file-upload/dist/ng-file-upload-shim.min.js");
require("./lib/ng-file-upload/dist/ng-file-upload.min.js");
require("./core/version/version.js");
require("./core/version/version-directive.js");
require("./core/version/interpolate-filter.js");

import normalize from './lib/html5-boilerplate/dist/css/normalize.css';
import boilerplate from './lib/html5-boilerplate/dist/css/main.css';
import bootstrap from './lib/bootstrap/dist/css/bootstrap.css';
import css from './app.css';

// Declare app level module which depends on views, and core components
let microblogApp = angular.module('microblogApp', [
    'angular-md5',
    'ngCookies',
    'ngSanitize',
    'ngFileUpload',
]);

microblogApp.controller('microblogController', [
    '$http',
    '$cookies',
    '$scope',
    'md5',
    '$filter',
    'Upload',
    function ($http, $cookies, $scope, md5, $filter, Upload) {

    let self = this;

    $scope.ATTACHMENT_URL = 1;
    $scope.ATTACHMENT_YOUTUBE = 2;
    $scope.ATTACHMENT_PICTURE = 3;

    $scope.apiUrl = 'http://test.shoka.com.ua/api/';
    $scope.lastPostId = 0;
    $scope.nickname = $cookies.get('user_nickname');
    $scope.user_hash = $cookies.get('user_hash');

    $scope.newlinks = [];
    $scope.newvideos = [];
    $scope.newfiles = [];

    $scope.getPosts = function () {
        $http.post($scope.apiUrl + 'posts',
            {user_hash: $scope.user_hash})
        .then(function (response) {
            $scope.posts = response.data.posts;
            if (response.data.posts.length) {
                $scope.lastPostId = response.data.posts[0].id;
            }
        });
    };

    $scope.sendNewPost = function ($event) {
        $event.preventDefault();

        if (!$scope.nickname) {
            alert('please set nickname');
            return;
        }

        if (!this.newPostContent && !$scope.newlinks.length && !$scope.newvideos.length && !$scope.newfiles.length) {
            alert('please add post content or attachment');
            return;
        }

        $cookies.put('user_nickname', this.nickname);
        if (!$scope.user_hash){
            let d = new Date;
            let md5Hash =  md5.createHash(this.nickname + this.newPostContent + d.getTime());
            $cookies.put('user_hash', md5Hash);
            $scope.user_hash = md5Hash;
            console.log('user hash generated');
        }

        let post_data = {
            user_nickname: $scope.nickname,
            user_hash: $scope.user_hash,
            content: $scope.newPostContent,
            attachments_links: $scope.newlinks,
            attachments_videos: $scope.newvideos,
            attachments_files: $scope.newfiles
        };

        Upload.upload({
            url: $scope.apiUrl + 'posts/add',
            data: post_data,
        }).
        then(function (response) {
            $scope.newPostContent = '';
            $scope.newlinks = [];
            $scope.newvideos = [];
            $scope.newfiles = [];

            $scope.getPosts();
        });

    };

    $scope.deletePost = function (post_id) {
        if (!confirm('Are you sure to delete post?')) {
            return;
        };

        $http.post($scope.apiUrl + 'posts/delete/' + post_id,
             {user_hash: $scope.user_hash}).
        then(function (response) {
            $scope.getPosts();
        });
    };

    $scope.toggleLike = function (post_id) {

        $http.post($scope.apiUrl + 'posts/like/' + post_id,
            {user_hash: $scope.user_hash}).
        then(function (response) {
            if (response.data.result != 1) {
                alert('Error ' + response.data.error);
                return;
            }

            //update likes counter for post
            let posts_filtered = $filter('filter')($scope.posts, {'id': post_id}, true);
            if (posts_filtered.length) {
                posts_filtered[0].likes_count = response.data.likes_count;
            }

        });
    };

    $scope.addNewLinkField = function (event) {
        event.preventDefault();
        $scope.newlinks.push({
            url: ''
        });
    };

    $scope.addNewVideoField = function (event) {
        event.preventDefault();
        $scope.newvideos.push({
            url: ''
        });
    };

    $scope.addNewFileField = function (event) {
        event.preventDefault();
        $scope.newfiles.push({
            file: false
        });
    };


    function refreshPostsByTimeout(){
        $scope.getPosts();
    }
    setInterval(refreshPostsByTimeout, 5000);

    $scope.getPosts();
}]);


microblogApp.directive('feed',  function () {
   return {
       template: require('./feed/feed.html'),
   }
});

microblogApp.directive('newPost', function () {
    return {
        template: require('./new-post/new-post.html'),
    }
});