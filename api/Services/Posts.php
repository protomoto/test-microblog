<?php
/**
 * Created by PhpStorm.
 * User: mukol
 * Date: 29.03.2020
 * Time: 14:28
 */
namespace Services;

use Models;
use Core\Container;
use Repositories\PostRepository;

/**
 * Class Posts
 * @package Services
 */
class Posts
{
    const RESULT_SUCCESS = 1;
    const RESULT_ERROR = 0;

    /**
     * Create post and attachment
     *
     * @param $uri_vars
     * @param $body_vars
     */
    public function addPost($uri_vars, $body_vars)
    {
        $user = Container::get()->user;
        if (isset($body_vars['user_hash'])) {
            $user_id = $user->findOrCreateUser($body_vars['user_hash'], $body_vars['user_nickname']);
            $user->loadUserById($user_id);
        }

        $postRepository = new PostRepository();
        $post_id = $postRepository->addPostWithAttachments($user, $body_vars);

        Container::get()->renderer->render(['post_id' => $post_id]);
    }

    /**
     * Get latest posts
     *
     * @param $uri_vars
     * @param $body_vars
     */
    public function getPosts($uri_vars, $body_vars)
    {
        $user = Container::get()->user;
        if (isset($body_vars['user_hash'])) {
            $user_id = $user->findOrCreateUser($body_vars['user_hash']);
            $user->loadUserById($user_id);
        }

        $postRepository = new PostRepository();
        $posts = $postRepository->getPostsWithAttachments(100, $user);

        $lastPostId = 0;
        if ($posts){
            $lastPostId = $posts[0]['id'];
        }
        Container::get()->renderer->render(['posts' => $posts, 'last_post_id' => $lastPostId]);
    }

    /**
     * Delete post
     *
     * @param $uri_vars
     * @param $body_vars
     */
    public function deletePost($uri_vars, $body_vars)
    {
        $user = Container::get()->user;
        if (isset($body_vars['user_hash'])){
            $user_id = $user->findOrCreateUser($body_vars['user_hash']);
            $user->loadUserById($user_id);
        } else {
            Container::get()->renderer->render(['result' => self::RESULT_ERROR]);
            return;
        }

        $post_id = (int)$uri_vars['post_id'];
        $posts = new Models\Posts();
        $data = $posts->getPost($post_id);

        if ($data && $data['user_id'] == $user->id) {

            //to remove post and assets we should call PostRepository
            $postRepository = new PostRepository();
            $postRepository->deletePost($post_id);

            Container::get()->renderer->render(['result' => self::RESULT_SUCCESS]);
        } else {
            Container::get()->renderer->render(['result' => self::RESULT_ERROR]);
            return;
        }
    }

    /**
     * Add or delete like to post from user
     * 
     * @param $uri_vars
     * @param $body_vars
     * @return bool|void
     */
    public function toggleLike($uri_vars, $body_vars)
    {
        $post_id = (int)$uri_vars['post_id'];
        $postModel = new Models\Posts();
        $post = $postModel->getPost($post_id);
        $user =  Container::get()->user;
        if (isset($body_vars['user_hash'])){
            $user_id = $user->findOrCreateUser($body_vars['user_hash']);
            $user->loadUserById($user_id);
        } else {
            Container::get()->renderer->render(['result' => self::RESULT_ERROR, 'error' => 'user not set']);
            return;
        }

        //check if author try to add like to own post
        if ($post['user_id'] == $user->id) {
            Container::get()->renderer->render(['result' => self::RESULT_ERROR, 'error' => 'user can not like own post', 'likes_count' => $post['likes_count']]);
            return false;
        }

        $postRepository = new PostRepository();
        $likeIsSet = $postRepository->toggleLike($user, $post_id);
        $post = $postModel->getPost($post_id);
        Container::get()->renderer->render(['result' => self::RESULT_SUCCESS, 'like_set' => $likeIsSet, 'likes_count' => $post['likes_count']]);
    }



}