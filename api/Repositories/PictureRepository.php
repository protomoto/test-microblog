<?php
/**
 * Created by PhpStorm.
 * User: mukol
 * Date: 01.04.2020
 * Time: 23:22
 */

namespace Repositories;

use Models\PictureAttachment;

/**
 * Class PictureRepository
 * @package Repositories
 */
class PictureRepository extends BaseAttachmentRepository
{
    /**
     * folder ro stire pictures
     *
     * @var string
     */
    private $upload_folder;

    /**
     * public url
     *
     * @var string
     */
    private $url_folder;

    /**
     * PictureRepository constructor.
     */
    public function __construct()
    {
        $this->attachment_type = self::ATTACHMENT_PICTURE;
        $this->var_key = 'attachments_files';
        $this->field_key = 'file';
        $this->upload_folder = PATH . 'test/upload/';
        $this->url_folder = '/upload/';

    }

    /**
     * @param $data
     * @return string
     */
    public function storePicture($data) {
        $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
        $new_filename = md5($data['name'] . time()) . '.' . $extension;
        move_uploaded_file($data['tmp_name'], $this->upload_folder . $new_filename);
        return $new_filename;
    }

    /**
     * @param $filename
     * @return string
     */
    public function getPictureUrl($filename) {
        $url = 'http://' . $_SERVER['SERVER_NAME'] . $this->url_folder . $filename;
        return $url;
    }

    /**
     * @param $filename
     */
    public function removePicture($filename){
        if ($filename && file_exists($this->upload_folder . $filename)) {
            unlink($this->upload_folder . $filename);
        }
    }

    /**
     * @param $data
     */
    public function removeAttachmentAssets($data){
        $this->removePicture($data['attachment_value']);
    }

    /**
     * prepare view of attachment
     *
     * @param $data
     * @return bool|string
     */
    public function renderAttachment($data) {
        if ($data['attachment_type'] == $this->attachment_type) {
            $url = $this->getPictureUrl($data['attachment_value']);
            return $url;
        } else {
            return false;
        }
    }

    /**
     * @param $data
     * @return bool|mixed|string
     */
    public function validateInputData($data)
    {
        $allowed_mime_types = ['image/jpg', 'image/jpeg', 'image/png'];

        $mime_type = mime_content_type($data['tmp_name']);
        if (!in_array($mime_type, $allowed_mime_types)) {
            return false;
        }

        $filename = $this->storePicture($data);
        return $filename;
    }

    /**
     * @param $data
     * @param $post_id
     * @return int
     */
    public function saveAttachments($data, $post_id)
    {
        $pictureAttachment = new PictureAttachment();
        return $pictureAttachment->saveAttachments($data, $this->attachment_type, $post_id);
    }

    /**
     * parse all data and if key is correct and data is valid - save the attachment
     *
     * @param array $vars
     * @param int $post_id
     * @return bool|int
     */
    public function parseInputData(array $vars, int $post_id)
    {
        $data = [];

        $vars = $_FILES;
        foreach ($vars as $key=>$value_arr) {
            if ($key == $this->var_key) {
                for ($i = 0; $i < count($value_arr['name']); $i++ ){

                    $file = [
                        'name'  =>  $value_arr['name'][$i][$this->field_key],
                        'tmp_name'  =>  $value_arr['tmp_name'][$i][$this->field_key],
                    ];

                    $fileValidated = $this->validateInputData($file);

                    if ($file) {
                        $data[] = $fileValidated;
                    }
                }
            }
        }

        if (count($data)) {
            return $this->saveAttachments($data, $post_id);
        } else {
            return false;
        }
    }
}