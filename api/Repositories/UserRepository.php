<?php
namespace Repositories;

use Models\Users;

/**
 * Class UserRepository
 *
 *
 * @package Repositories
 */
class UserRepository
{
    /** loaded user id
     * @var
     */
    public $id;

    /**
     * loaded user nickname
     *
     * @var
     */
    public $nickname;

    /**
     * loaded user hash
     *
     * @var
     */
    public $hash;

    /**
     * users model object
     *
     * @var Users
     */
    private $userModel;

    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->userModel = new Users();
    }

    /**
     *
     * @param $data
     */
    public function loadUser($data)
    {
        $this->id = $data['id'];
        $this->nickname = $data['nickname'];
        $this->hash = $data['hash'];
    }

    /**
     * @param int $id
     * @return bool
     */
    public function loadUserById(int $id)
    {
        $data = $this->userModel->getUserById($id);
        if ($data){
            $this->loadUser($data);
            return true;
        } else {
            return false;
        }

    }

    /**
     * Find user by nickname.
     * if user found, check and update nickname
     * if not found - create user and set nickname
     *
     * @param $userHash
     * @param string $nickname
     * @return int
     */
    public function findOrCreateUser($userHash, $nickname = '')
    {
        $data = $this->userModel->getUserByHash($userHash);
        if ($data) {
            if (($data['nickname'] != $nickname) && ($nickname != '')) {
                $this->userModel->updateUserName($data['id'], $nickname);
            }
            return $data['id'];
        } else {
            return $this->userModel->createUser($userHash, $nickname);
        }
    }
}