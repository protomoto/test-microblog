<?php
namespace Repositories;

/**
 * Class UrlRepository
 * @package Repositories
 */
class UrlRepository extends BaseAttachmentRepository
{
    /**
     * UrlRepository constructor.
     */
    public function __construct()
    {
        $this->attachment_type = self::ATTACHMENT_URL;
        $this->var_key = 'attachments_links';
        $this->field_key = 'url';
    }

    /**
     * @param $data
     * @return bool|string
     */
    public function renderAttachment($data)
    {
        if ($data['attachment_type'] == $this->attachment_type) {
            return $data['attachment_value'];
        } else {
            return false;
        }
    }

    /**
     * @param $data
     * @return bool|mixed|string
     */
    public function validateInputData($data)
    {
        //save youtube video code or fail
        if (preg_match('%(http|https):\/\/(.+)%i', $data, $match)) {
            $data = strip_tags($data);
            $data = htmlentities($data, ENT_QUOTES, "UTF-8");
            $data = htmlspecialchars($data, ENT_QUOTES);
            return $data;
        } else {
            return false;
        }
    }
}