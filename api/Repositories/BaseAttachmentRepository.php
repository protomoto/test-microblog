<?php
/**
 * Created by PhpStorm.
 * User: mukol
 * Date: 02.04.2020
 * Time: 1:34
 */

namespace Repositories;

use Models\Posts;

/**
 * Class BaseAttachmentRepository
 * @package Repositories
 */
abstract class BaseAttachmentRepository
{
    /*
     * constants for database and detection attachment type
     */
    const ATTACHMENT_URL = 1;
    const ATTACHMENT_YOUTUBE = 2;
    const ATTACHMENT_PICTURE = 3;

    /**
     * attachment type for current class
     * @var int
     */
    protected $attachment_type;

    /**
     * variable name to search in vars input array for attachment
     *
     * @var string
     */
    protected $var_key;

    /**
     * main data field in attachment array
     * for example "url"
     *
     * @var string
     */
    protected $field_key;

    /**
     * prepare view of attachment
     *
     * @param $data
     * @return string
     */
    public abstract function renderAttachment($data);

    /**
     * validates does data can be stored
     *
     * @param $data
     * @return bool|mixed
     */
    public abstract function validateInputData($data);

    /**
     * Delete stored files or other specific data
     *
     * @param $data
     */
    public function removeAttachmentAssets($data) {

    }

    /**
     * parse all data and if key is correct and data is valid - save the attachment
     *
     * @param array $vars
     * @param int $post_id
     * @return bool|
     */
    public function parseInputData(array $vars, int $post_id){
        $data = [];
        foreach ($vars as $key=>$value_arr) {
            if ($key == $this->var_key) {
                foreach ($value_arr as $single_value) {
                    $validatedData = $this->validateInputData($single_value[$this->field_key]);
                    if ($validatedData !== false) {
                        $data[] = $validatedData;
                    }
                }
            }
        }

        if (count($data)) {
            return $this->saveAttachments($data, $post_id);
        } else {
            return false;
        }
    }

    /**
     * @param $data
     * @param $post_id
     * @return int
     */
    public function saveAttachments($data, $post_id)
    {
        $post = new Posts();
        foreach ($data as $val) {
            $post->addAttachment($val, $this->attachment_type, $post_id);
        }
        return count($data);
    }
}