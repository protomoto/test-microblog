<?php
namespace Repositories;

use Models\Posts;

/**
 * Class PostRepository
 * @package Repositories
 */
class PostRepository
{
    const POST_NOT_EXISTS = -1;
    const LIKE_ADDED = 1;
    const LIKE_REMOVED = 0;

    /**
     * post model shorthand
     *
     * @var Posts
     */
    private $postModel;

    /**
     * PostRepository constructor.
     */
    public function __construct()
    {
        $this->postModel = new Posts();
    }

    /**
     * Create post, store attachments for post
     *
     * @param $user
     * @param $body_vars
     * @return int
     */
    public function addPostWithAttachments($user, $body_vars)
    {
        $content = (isset($body_vars['content']) ? $body_vars['content'] : '');
        $post_id = $this->postModel->addPost($user, $content);

        if ($post_id) {
            $urlRepository = new UrlRepository();
            $youtubeRepository = new YoutubeRepository();
            $pictureRepository = new PictureRepository();
            foreach ([$urlRepository, $youtubeRepository, $pictureRepository] as $attachmentRepository) {
                $attachmentRepository->parseInputData($body_vars, $post_id);
            }
        }

        return $post_id;
    }

    /**
     * Retrieve posts and all attachments
     *
     * @param int $limit
     * @return array
     */
    public function getPostsWithAttachments($limit = 100, UserRepository $user = null)
    {
        $posts = $this->postModel->getPosts($limit, $user);

        $posts_id = array_column($posts, 'id');
        if (count($posts_id)) {
            $attachments = $this->postModel->getAttachments($posts_id);

            $urlRepository = new UrlRepository();
            $youtubeRepository = new YoutubeRepository();
            $pictureRepository = new PictureRepository();

            foreach ($posts as &$post){
                $post['content'] = str_replace('\n', '<br>', $post['content']);
                $post['date_added_html'] = date('Ymd\TH:i:s',strtotime($post['date_added']));
                foreach ($attachments as $attachment) {
                    if ($post['id'] == $attachment['post_id']) {
                        if (!isset($post['attachments'])) {
                            $post['attachments'] = [];
                        }

                        foreach ([$urlRepository, $youtubeRepository, $pictureRepository] as $attachmentRepository) {
                            $preview = $attachmentRepository->renderAttachment($attachment);
                            if (false != $preview) {
                                $attachment['preview'] = $preview;
                                continue;
                            }
                        }

                        $post['attachments'][] = $attachment;
                    }
                }
            }
            unset($post);
        }

        return $posts;
    }

    /**
     * set or delete like for post from user
     *
     * @param UserRepository $user
     * @param int $post_id
     * @return int
     */
    public function toggleLike(UserRepository $user, int $post_id)
    {
        $user_id = $user->id;
        $post = $this->postModel->getPost($post_id);
        if (!$post){
            return self::POST_NOT_EXISTS;
        }

        $like = $this->postModel->getPostLike($post_id, $user_id);

        if ($like) {
            //remove like
            $this->postModel->deleteLike($like['id']);
            return self::LIKE_REMOVED;
        } else {
            //add like
            $this->postModel->addLike($user_id, $post_id);
            return self::LIKE_ADDED;
        }

    }

    /**
     * Remove post, likes and all attachments
     *
     * @param $post_id
     */
    public function deletePost($post_id)
    {
        //delete likes
        $this->postModel->deleteLikes($post_id);

        //delete attachments
        $attachments = $this->postModel->getAttachments($post_id);

        $urlRepository = new UrlRepository();
        $youtubeRepository = new YoutubeRepository();
        $pictureRepository = new PictureRepository();
        foreach ($attachments as $attachment){
            foreach ([$urlRepository, $youtubeRepository, $pictureRepository] as $attachmentRepository) {
                $attachmentRepository->removeAttachmentAssets($attachment);
            }
        }

        $this->postModel->deleteAttachments($post_id);

        //delete post
        $this->postModel->deletePost($post_id);
    }
}