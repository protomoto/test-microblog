<?php
namespace Repositories;

/**
 * Class YoutubeRepository
 * @package Repositories
 */
class YoutubeRepository extends BaseAttachmentRepository
{
    /**
     * YoutubeRepository constructor.
     */
    public function __construct()
    {
        $this->attachment_type = self::ATTACHMENT_YOUTUBE;
        $this->var_key = 'attachments_videos';
        $this->field_key = 'url';
    }

    /**
     * @param $data
     * @return bool|string
     */
    public function renderAttachment($data) {
        if ($data['attachment_type'] == $this->attachment_type) {
            return $data['attachment_value'];
        } else {
            return false;
        }
    }

    /**
     * @param $data
     * @return bool|mixed
     */
    public function validateInputData($data)
    {
        //save youtube video code or fail
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/\s]{11})%i', $data, $match)) {
            return $match[1];
        } else {
            return false;
        }
    }
}