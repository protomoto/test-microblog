<?php
/**
 * Created by PhpStorm.
 * User: mukol
 * Date: 29.03.2020
 * Time: 13:01
 */
ini_set('display_errors', 1);
error_reporting(E_ALL);

header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Origin: *');

require 'bootstrap.php';

$container->router->addRule('get', '/api/users', 'Services\Users@getUsers');
$container->router->addRule('post', '/api/posts', 'Services\Posts@getPosts');
$container->router->addRule('post', '/api/posts/add', 'Services\Posts@addPost');
$container->router->addRule('post', '/api/posts/delete/{post_id}', 'Services\Posts@deletePost');
$container->router->addRule('post', '/api/posts/like/{post_id}', 'Services\Posts@toggleLike');

$container->router->dispatch();
