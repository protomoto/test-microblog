<?php
/**
 * Bootstrap for framework
 *
 * Initiates container and basic objects, creates DB connection
 */
define('PATH' , __DIR__  . '/../../' );

require PATH . 'vendor/autoload.php';

$microblogAutoloader = function ($class){
    include str_replace("\\" , DIRECTORY_SEPARATOR , '../../' . $class  ). '.php';
};

spl_autoload_register($microblogAutoloader, false);

use Core\{Container, Router, JsonRenderer, Database};
use Repositories\UserRepository;

$router = new Router();
$jsonRenderer = new JsonRenderer();
$database = new Database();

$container = Container::get();
$container->setDatabase($database->getDatabaseDriver());
$container->setRouter($router);
$container->setRenderer($jsonRenderer);

$user = new UserRepository();
$container->setUser($user);