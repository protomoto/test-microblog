<?php
/**
 * Created by PhpStorm.
 * User: mukol
 * Date: 29.03.2020
 * Time: 17:26
 */
namespace Models;

/**
 * Class Users
 * @package Models
 */
class Users extends BaseModel
{
    /**
     * Retrieves user from database by id
     *
     * @param int $id
     * @return array
     */
    public function getUserById(int $id)
    {
        $this->database->where("id", $id);
        return $this->database->getOne("users");
    }

    /**
     * Retrieves user from database by hash
     *
     * @param $hash
     * @return array
     */
    public function getUserByHash($hash)
    {
        $this->database->where("hash", $hash);
        return $this->database->getOne("users");
    }

    /**
     * Store new user
     *
     * @param $userHash
     * @param $nickname
     * @return int - new user id
     */
    public function createUser($userHash, $nickname)
    {
        return $this->database->insert('users',[
            'nickname' => $this->database->escape($this->sanitizeString($nickname)),
            'hash' => $this->database->escape($this->sanitizeString($userHash))
        ]);
    }

    /**
     * Update user nickname by user id
     *
     * @param $id
     * @param $nickname
     */
    public function updateUserName($id, $nickname)
    {
        $this->database->where('id', $id);
        $this->database->update('users', [
            'nickname' => $this->database->escape($this->sanitizeString($nickname))
        ]);
    }
}