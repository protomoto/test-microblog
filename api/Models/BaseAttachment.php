<?php
/**
 * Created by PhpStorm.
 * User: mukol
 * Date: 31.03.2020
 * Time: 1:10
 */
namespace Models;

/**
 * Class BaseAttachment
 * base class for processing attachments to posts
 *
 * @package Models
 */
abstract class BaseAttachment
{
    /**
     * process attachment to post
     *
     * @param $data
     * @param $attachment_type
     * @param $post_id
     * @return int
     */
    public function saveAttachments($data, $attachment_type, $post_id)
    {
        $post = new Posts();
        foreach ($data as $val) {
            $post->addAttachment($val, $attachment_type, $post_id);
        }
        return count($data);
    }
}