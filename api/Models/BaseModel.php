<?php
/**
 * Created by PhpStorm.
 * User: mukol
 * Date: 29.03.2020
 * Time: 17:31
 */
namespace Models;

use Core;

/**
 * Class BaseModel
 * @package Models
 */
abstract class BaseModel
{
    /**
     * shorthand for database object
     *
     * @var
     */
    protected $database;

    /**
     * BaseModel constructor.
     */
    public function __construct()
    {
        $this->database = Core\Container::get()->database;
    }

    /**
     * safe clear function for string values
     *
     * @param $string
     * @return string
     */
    public function sanitizeString($string)
    {
        $string = strip_tags($string,'<br>');
        $string = htmlentities($string, ENT_QUOTES, "UTF-8");
        $string = htmlspecialchars($string, ENT_QUOTES);

        return $string;
    }
}