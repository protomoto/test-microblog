<?php
namespace Models;

use Repositories\UserRepository;

/**
 * Class Posts
 * @package Models
 */
class Posts extends BaseModel
{
    /**
     * Retrieve base post information from database by post id
     * Does not consist attachments
     *
     * @param int $post_id
     * @return mixed
     */
    public function getPost(int $post_id)
    {
        $this->database->where('p.id', $post_id);
        $this->database->join('users u', 'u.id = p.user_id', 'LEFT');
        $this->database->join('likes', 'likes.post_id = p.id', 'LEFT');
        $this->database->groupby('p.id');
        $this->database->orderby('p.id', 'DESC');
        $post = $this->database->getOne('posts p', 'p.*, u.nickname, count(likes.id) as likes_count');

        return $post;
    }

    /**
     * Retrieves last posts from database
     *
     * @param int $limit
     * @param UserRepository|null $user - if pass, adds property 'can_delete' to result array
     * @return mixed
     */
    public function getPosts($limit = 100, UserRepository $user = null)
    {
        $can_delete = '';
        if (isset($user)) {
            $can_delete = ', CASE 
                WHEN u.hash = "' . $user->hash . '" 
                THEN 1 
                ELSE 0 
            END as can_delete';
        }
        $this->database->join('users u', 'u.id = p.user_id', 'LEFT');
        $this->database->join('likes', 'likes.post_id = p.id', 'LEFT');
        $this->database->groupby('p.id');
        $this->database->orderby('p.id', 'DESC');
        $posts = $this->database->get('posts p', $limit, 'p.*, u.nickname, count(likes.id) as likes_count' . $can_delete);
        return $posts;
    }

    /**
     * Store post to database
     *
     * @param $user
     * @param $content
     * @return int
     */
    public function addPost($user, $content): int
    {
        return $this->database->insert('posts', [
            'user_id' => $user->id,
            'content' => $this->database->escape($this->sanitizeString($content))
            ]);
    }

    /**
     * Deletes post from database
     *
     * @param $post_id
     */
    public function deletePost($post_id)
    {
        $this->database->where('id', $post_id);
        $this->database->delete('posts', 1);
    }

    /**
     * Store attachment to database
     *
     * @param string $value
     * @param int $attachment_type
     * @param int $post_id
     */
    public function addAttachment($value, $attachment_type, $post_id)
    {
        $this->database->insert('attachments',[
            'post_id'   => (int)$post_id,
            'attachment_type' => $this->database->escape($this->sanitizeString($attachment_type)),
            'attachment_value' => $this->database->escape($this->sanitizeString($value))
        ]);
    }

    /**
     * Deletes attachments from database
     *
     * @param $post_id
     */
    public function deleteAttachments($post_id)
    {
        $this->database->where('post_id', $post_id);
        $this->database->delete('attachments');
    }

    /**
     * Retrieves all attachments from database assigned to post_id(s)
     *
     * @param int|array $post_ids
     * @return mixed
     */
    public function getAttachments($post_ids)
    {
        if (!is_array($post_ids)) {
            $post_ids = [$post_ids];
        }
        $this->database->where('post_id', $post_ids, 'in');
        $this->database->orderBy('id', 'asc');
        $attachments = $this->database->get('attachments');
        return $attachments;
    }

    /**
     * Retrieves information did user_id like post_id
     *
     * @param int $post_id
     * @param int $user_id
     * @return mixed
     */
    public function getPostLike($post_id, $user_id)
    {
        $this->database->where('post_id', $post_id);
        $this->database->where('user_id', $user_id);
        $like = $this->database->getOne('likes');
        return $like;
    }

    /**
     * Store like for post_id from user_id
     *
     * @param int $user_id
     * @param int $post_id
     * @return int
     */
    public function addLike($user_id, $post_id)
    {
        return $this->database->insert('likes', [
            'user_id' => $user_id,
            'post_id' => $post_id
        ]);
    }

    /**
     * Delete like by id
     *
     * @param $like_id
     */
    public function deleteLike($like_id)
    {
        $this->database->where('id', $like_id);
        $this->database->delete('likes');
    }

    /**
     * Delete all likes for post_id
     *
     * @param $post_id
     */
    public function deleteLikes($post_id)
    {
        $this->database->where('post_id', $post_id);
        $this->database->delete('likes');
    }

}