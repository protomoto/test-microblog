<?php
/**
 * Created by PhpStorm.
 * User: mukol
 * Date: 29.03.2020
 * Time: 19:05
 */

namespace Core;


/**
 * Class JsonRenderer
 *
 * render data into json array and print result
 *
 * @package Core
 */
class JsonRenderer implements RendererInterface
{
    /**
     * implements RenderInterface render function
     *
     * @param array $data
     * @param null $template
     */
    public function render(array $data, $template = null)
    {
        echo json_encode($data);
    }
}