<?php
namespace Core;

/**
 * Class Database
 * @package Core
 */
class Database
{
    /**
     * Creates instance of current database driver
     * @return \MysqliDb
     */
    public function getDatabaseDriver()
    {
        //does not support autoloader
        require_once(PATH . '/vendor/thingengineer/mysqli-database-class/MysqliDb.php');

        return new \MysqliDb('darksite.mysql.tools', 'darksite_test', 'ixY(*34L4s', 'darksite_test');
    }

}