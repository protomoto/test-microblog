<?php
namespace Core;

use Repositories\UserRepository;
/**
 * Class Container
 * @package Core
 */
class Container
{

    /**
     * singletone instance
     * @var
     */
    private static $instance;

    /**
     * object of database class
     * @var
     */
    public $database;

    /**
     * Instance of RouterInterface implemented object
     *
     * @var \Core\Router
     */
    public $router;

    /**
     * instance of UserRepository
     * @var \Repositories\UserRepository
     */
    public $user;

    /**
     * Instance of RenderInterface implemented object
     * @var \Core\RendererInterface
     */
    public $renderer;

    /**
     * singleton function for container
     * @return Container
     */
    public static function get()
    {
        if (isset(self::$instance)) {
            return self::$instance;
        }
        self::$instance = new self();
        return self::$instance;
    }

    /**
     * database setter
     *
     * @param $databaseObject
     */
    public function setDatabase($databaseObject)
    {
        $this->database = $databaseObject;
    }

    /**
     * router setter
     *
     * @param $routerObject
     */
    public function setRouter(RouterInterface $routerObject)
    {
        $this->router = $routerObject;
    }

    /**
     * renderer setter
     *
     * @param RendererInterface $rendererObject
     */
    public function setRenderer(RendererInterface $rendererObject)
    {
        $this->renderer = $rendererObject;
    }

    /**
     * user setter
     *
     * @param $userRepository
     */
    public function setUser(UserRepository $userRepository)
    {
        $this->user = $userRepository;
    }

}