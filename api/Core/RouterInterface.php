<?php
namespace Core;

/**
 * Interface RouterInterface
 * @package Core
 */
interface RouterInterface
{
    /**
     * setter for rules
     *
     * @param string $method
     * @param string $pattern
     * @param string $action
     * @return mixed
     */
    public function addRule(string $method, string $pattern, string $action);

    /**
     * main router dispatcher
     *
     * @return mixed
     */
    public function dispatch();
}