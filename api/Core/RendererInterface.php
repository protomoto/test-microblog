<?php
/**
 * Created by PhpStorm.
 * User: mukol
 * Date: 29.03.2020
 * Time: 19:04
 */
namespace Core;

/**
 * Interface RendererInterface
 * @package Core
 */
interface RendererInterface
{
    /**
     * main render function
     *
     * @param array $data - array of data to render
     * @param null $template - template url
     * @return mixed
     */
    public function render(array $data, $template = null);
}