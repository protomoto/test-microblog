<?php
declare(strict_types=1);

namespace Core;

/**
 * Class Router
 * Main routes dispatcher
 *
 * @package Core
 */
class Router implements RouterInterface
{

    /**
     * set of rules for GET request
     *
     * @var array
     */
    private $get_rules = [];

    /**
     * set of rules for POST request
     *
     * @var array
     */
    private $post_rules = [];

    /**
     * set of rules for DELETE request
     *
     * @var array
     */
    private $delete_rules = [];

    /**
     * current uri
     *
     * @var
     */
    private $uri;

    /**
     * getter for uri property
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->uri  = $_SERVER['REQUEST_URI'];
    }

    /**
     * setter for rules
     *
     * @param string $method
     * @param string $pattern
     * @param string $action
     */
    public function addRule(string $method, string $pattern, string $action): void
    {
        $rulesPointer = false;
        switch ($method) {
            case 'get':
                        $rulesPointer = &$this->get_rules;
                        break;
            case 'post':
                        $rulesPointer = &$this->post_rules;
                        break;
            case 'delete':
                        $rulesPointer = &$this->delete_rules;
                        break;
            default: return;
        }

        $rulesPointer[] = [
            'pattern' => $pattern,
            'action'  => $action
        ];

    }

    /**
     * executes action and pass request parameters
     *
     * @param string $routeAction
     * @param array $uri_vars
     * @param array $body_vars
     */
    private function callRouteAction(string $routeAction, array $uri_vars, array $body_vars)
    {
        $actionArray = explode('@', $routeAction);
        list($controller, $action) = $actionArray;
        try {
            $controllerObject = new $controller;
            if (!method_exists($controllerObject, $action)) {
                throw new \Exception('Method ' . $action . ' not exists in class ' . $controller);
            }
            call_user_func(array($controllerObject, $action), $uri_vars, $body_vars);
        } catch (\Exception $e) {
            echo 'Error call action ' . (string)$routeAction . ' : ' . $e->getMessage();
        }
    }

    /**
     * main router dispatcher
     *
     * @return bool
     * @throws \Exception
     */
    public function dispatch()
    {
        $uri_vars = [];
        $body_vars = [];

        $rules = $this->get_rules;
        if ('POST' == $_SERVER['REQUEST_METHOD']) {
            $rules = $this->post_rules;
        } elseif ('DELETE' == $_SERVER['REQUEST_METHOD']) {
            $rules = $this->delete_rules;
        }

        if (count($_POST)) {
            $body_vars = $_POST;
        } else {
            //angularjs workaround
            $body_vars = json_decode(file_get_contents('php://input'), true);
        }

        foreach ($rules as $rule) {
            $params_map = [];
            $pattern = str_replace('/','\\/', $rule['pattern']);

            $single_param_pattern = '*\{([^/]+)\}*';
            $clean_pattern = preg_replace($single_param_pattern, '(.+)', $pattern);

            $matches = [];

            if (preg_match('*^' . $clean_pattern . '$*', $this->uri, $matches) != false){
                preg_match_all($single_param_pattern, $pattern, $params_map);

                foreach ($params_map[1] as $key => $param_name){
                    $uri_vars[$param_name] = $matches[$key + 1];
                }

                $this->callRouteAction($rule['action'], $uri_vars, $body_vars);
                return true;
            }
        }

        throw new \Exception('Route not detected');
    }
}